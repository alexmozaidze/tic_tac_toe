use macroquad::prelude::*;

#[rustfmt::skip]
pub fn draw_x(x: f32, y: f32, w: f32, h: f32, thickness: f32, color: Color) {
    draw_line(x,   y, x+w, y+h, thickness, color); // upper-left to bottom-right
    draw_line(x+w, y, x,   y+h, thickness, color); // upper-right to bottom-left
}

pub fn draw_o(x: f32, y: f32, w: f32, _h: f32, thickness: f32, color: Color) {
    // FIXME: Doesn't draw correctly on non-rectangle targets.
    // TODO: Draw elipse instead of a circle
    draw_circle_lines(x + (w / 2.), y + (w / 2.), w / 2., thickness, color);
}

pub fn draw_separator_lines(
    target_width: f32,
    target_height: f32,
    cell_width: f32,
    cell_height: f32,
    thickness: f32,
    color: Color,
) {
    // Vertical lines
    draw_line(cell_width, 0., cell_width, target_height, thickness, color);
    draw_line(
        cell_width + cell_width,
        0.,
        cell_width + cell_width,
        target_height,
        thickness,
        color,
    );

    // Horizontal lines
    draw_line(0., cell_height, target_width, cell_height, thickness, color);
    draw_line(
        0.,
        cell_height + cell_height,
        target_width,
        cell_height + cell_height,
        thickness,
        color,
    );
}
