use crate::board_sign::BoardSign;

#[derive(Default, Clone, Debug)]
pub struct Score {
    pub row: [isize; 3],
    pub col: [isize; 3],
    pub diag: [isize; 2],
}

impl Score {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_flattened(&self) -> [isize; 8] {
        [
            self.row[0],
            self.row[1],
            self.row[2],
            self.col[0],
            self.col[1],
            self.col[2],
            self.diag[0],
            self.diag[1],
        ]
    }

    pub fn check_winner(&self) -> BoardSign {
        for count in self.get_flattened() {
            if count == 3 * BoardSign::X as isize {
                return BoardSign::X;
            } else if count == 3 * BoardSign::O as isize {
                return BoardSign::O;
            }
        }

        BoardSign::N
    }

}
