#![forbid(unsafe_code)]

mod board_sign;
mod draw;
mod score;

use macroquad::prelude::*;
use macroquad_canvas::Canvas2D;

use board_sign::BoardSign;
use draw::{draw_o, draw_separator_lines, draw_x};
use score::Score;

const DRAW_LINE_THICKNESS: f32 = 6.; // in pixels

type Board = [BoardSign; 9];

fn window_conf() -> Conf {
    Conf {
        window_title: "Tic Tac Toe -- Rust Edition 🦀".to_owned(),
        window_width: 500,
        window_height: 500,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    // NOTE: Must be a square. Otherwise, O's are not drawn properly
    let canvas = Canvas2D::new(1000., 1000.);

    let cell_width = canvas.width() / 3.;
    let cell_height = canvas.height() / 3.;
    let new_cell = |x, y| Rect {
        x,
        y,
        w: cell_width,
        h: cell_height,
    };

    let mut board: Board = [BoardSign::N; 9];
    #[rustfmt::skip]
    let board_cell_collisions = [
        new_cell(0.,                      0.),
        new_cell(cell_width,              0.),
        new_cell(cell_width + cell_width, 0.),

        new_cell(0.,                      cell_height),
        new_cell(cell_width,              cell_height),
        new_cell(cell_width + cell_width, cell_height),

        new_cell(0.,                      cell_height + cell_height),
        new_cell(cell_width,              cell_height + cell_height),
        new_cell(cell_width + cell_width, cell_height + cell_height),
    ];
    let mut score = Score::new();
    let mut player_turn = BoardSign::X;
    let mut winner: Option<BoardSign> = None;

    // A slightly more convenient way to update score
    macro_rules! edit_score {
        ($row:expr, $col:expr) => {{
            score.row[$row] += player_turn as isize;
            score.col[$col] += player_turn as isize;
        }};
        ($row:expr, $col:expr, $diag:expr) => {{
            edit_score!($row, $col);
            score.diag[$diag] += player_turn as isize;
        }};
        // Special case. Fills both diagonals
        ($row:expr, $col:expr, _) => {{
            edit_score!($row, $col, 0);
            score.diag[1] += player_turn as isize;
        }};
    }

    macro_rules! restart_game {
        () => {
            board = [BoardSign::N; 9];
            winner = None;
            player_turn = BoardSign::X;
            score = Score::new();
        };
    }

    loop {
        if is_key_pressed(KeyCode::Q) || is_key_pressed(KeyCode::Escape) {
            return;
        }

        if winner.is_some() && is_key_pressed(KeyCode::R) {
            restart_game!();
        }

        let (mouse_x, mouse_y) = canvas.mouse_position();

        // Mouse input
        for ((cell_index, cell), &collision) in board
            .into_iter()
            .enumerate()
            .zip(board_cell_collisions.iter())
        {
            if winner.is_some() {
                break;
            }

            if cell == BoardSign::N
                && is_mouse_button_pressed(MouseButton::Left)
                && collision.contains(vec2(mouse_x, mouse_y))
            {
                match cell_index {
                    0 => edit_score!(0, 0, 0),
                    1 => edit_score!(0, 1),
                    2 => edit_score!(0, 2, 1),
                    3 => edit_score!(1, 0),
                    4 => edit_score!(1, 1, _),
                    5 => edit_score!(1, 2),
                    6 => edit_score!(2, 0, 1),
                    7 => edit_score!(2, 1),
                    8 => edit_score!(2, 2, 0),
                    _ => unreachable!(),
                }

                board[cell_index] = player_turn;
                player_turn = !player_turn;

                if !board.contains(&BoardSign::N) {
                    winner = Some(BoardSign::N);
                }

                match score.check_winner() {
                    BoardSign::N => {}
                    BoardSign::X => winner = Some(BoardSign::X),
                    BoardSign::O => winner = Some(BoardSign::O),
                }
            }
        }

        set_camera(&canvas.camera);
        clear_background(RED);

        draw_separator_lines(
            canvas.width(),
            canvas.height(),
            cell_width,
            cell_height,
            DRAW_LINE_THICKNESS,
            BLACK,
        );
        for (cell, &collision) in board.iter().zip(board_cell_collisions.iter()) {
            match cell {
                BoardSign::N => {}
                BoardSign::X => draw_x(
                    collision.x,
                    collision.y,
                    collision.w,
                    collision.h,
                    DRAW_LINE_THICKNESS,
                    BLACK,
                ),
                BoardSign::O => draw_o(
                    collision.x,
                    collision.y,
                    collision.w,
                    collision.h,
                    DRAW_LINE_THICKNESS,
                    BLACK,
                ),
            }
        }

        if let Some(winner) = winner {
            let text_x = 4.;
            let winner_text_y = 4. + 62.;
            let restart_text_y = 8. + (62. * 2.);

            let winner_text = match winner {
                BoardSign::N => "Draw! Nobody won!",
                BoardSign::X => "Player X Won!",
                BoardSign::O => "Player O Won!",
            };

            draw_text(winner_text, text_x, winner_text_y, 62., WHITE);
            draw_text("Press R to restart", text_x, restart_text_y, 62., WHITE);
        }

        set_default_camera();
        canvas.draw();
        next_frame().await;
    }
}
