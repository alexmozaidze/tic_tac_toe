#[derive(PartialEq, Eq, Clone, Copy, Debug)]
#[repr(isize)]
pub enum BoardSign {
    N = 0, // None
    X = 1,
    O = -1,
}

impl std::ops::Not for BoardSign {
    type Output = Self;

    fn not(self) -> Self {
        match self {
            Self::N => panic!("Used \"Not\" operator on `BoardSign::N` (a `None` value)"),
            Self::X => Self::O,
            Self::O => Self::X,
        }
    }
}
