# Rusty Tic Tac Toe

A ridiculously simple Tic Tac Toe game written in Rust.

I decided to make it in order to learn game development and Rust.

Previous version of the game was very, very bad, so I rewrote it and removed a lot of dependencies.
It took me 1 day with frequent 1-2 hour breaks.

## Features

+ Uses Macroquad
+ Very simple
+ *Not* written in Python
+ Burns your eyes
+ Crab

## Screenshot

![In-game screenshot, see tic-tac-toe.png](tic-tac-toe.png "Exciting Gameplay!")

## Building

Execute following code in the scary black window of characters known as "terminal" in order to
launch the game.

``` sh
cargo run --release
```

**WARNING:** This was only tested on Arch Linux, it *might* work on Windows/Mac, but I didn't test it.


## Things Learned

+ When working with a global value, prefer, and I mean, **strongly** prefer borrowing it immutably or cloning it.
  Don't keep mutable borrows for long, as they're troublesome to get around.

+ Manually checking every cell for winning logic is the stupidest idea, I should've listened [MSN](https://stackoverflow.com/questions/4198955/how-to-find-the-winner-of-a-tic-tac-toe-game-of-any-size#4199023) from the start.

## Crab

As promised: 🦀

## Todo

+ [ ] Add some tests
+ [x] Refactor to make `main.rs` clean and easy to read
+ [ ] Draw elipses instead of circles
+ [ ] Remove `macroquad-canvas` because we are no longer bound by a radius
+ [ ] Optimize as a final touch
